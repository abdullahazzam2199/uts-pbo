# No 1
Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)

# No 2
Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)
[Link Coding Full](https://gitlab.com/abdullahazzam2199/uts-pbo/-/blob/main/UTS%20PBO/Koding_UTS.rar)

# No 3

Mampu menjelaskan konsep dasar OOP


[Video](https://www.youtube.com/watch?v=anBLiFFBUYA)

# No 4
Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)
# Class KumpulanSoal
```java
 protected ArrayList<String> soalUTBK = new ArrayList<String>();
    protected ArrayList<String> pilihanA = new ArrayList<String>();
    protected ArrayList<String> pilihanB = new ArrayList<String>();
    protected ArrayList<String> pilihanC = new ArrayList<String>();
    protected ArrayList<String> jawaban = new ArrayList<String>();

```

# No 5
Mampu mendemonstrasikan penggunaan Abstraction secara tepat (Lampirkan link source code terkait)
# Class Soal
```java
public abstract class Soal extends TampilanDashboard {

    public abstract void SetSoal();

}

```
```java
```

# No 6
Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)
# Class Materi
```java
class Materi extends TampilanDashboard {
```
# Class Soal
```java

import java.util.Scanner;

public abstract class Soal extends TampilanDashboard {

    public abstract void SetSoal();
}


```
# Class Kumpulan_Soal
```java
@Override
    public void SetSoal() {
        boolean putusan;
        putusan = true;
        Scanner input = new Scanner(System.in);

        while (putusan) {

            int pilihan;
            pilihan = 0;
            System.out.println("==========Pilihan Soal=========");
            System.out.println("1. Biologi");
            System.out.println("2. Kimia");
            System.out.println("3. Fisika");
            System.out.println("4. Matematika");
            System.out.println("5. Kembali");
            System.out.println("=================================");
            System.out.println("Apa pilihan anda?");

            pilihan = input.nextInt();
            switch (pilihan) {
                case 1:
                    Kumpulan_Soal objek = new SoalBiologi();
                    objek.SetSoal();
                    break;
                case 2:
                    Kumpulan_Soal objek1 = new SoalKimia();
                    objek1.SetSoal();
                    break;
                case 3:
                    Kumpulan_Soal objek2 = new SoalFisika();
                    objek2.SetSoal();
                    break;
                case 4:
                    Kumpulan_Soal objek3 = new SoalMatematika();
                    objek3.SetSoal();
                    break;
                case 5:
                    putusan = false;

            }
        }
    }

}
```
# Class SoalFisika
```java
@Override
    public void SetSoal() {
        Scanner input = new Scanner(System.in);
        Scanner input1 = new Scanner(System.in);
        boolean putusan;
        putusan = true;

        soalUTBK.add(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget euismod erat. Lorem ipsum dolor?");
        pilihanA.add("Lorem ipsum");
        pilihanB.add("dolor sit ");
        pilihanC.add("amet, consectetur");
        jawaban.add("A");

        soalUTBK.add(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget euismod erat. Lorem ipsum dolor?");
        pilihanA.add("Lorem ipsum");
        pilihanB.add("dolor sit ");
        pilihanC.add("amet, consectetur");
        jawaban.add("B");
        while (putusan) {

            int pilihan, pilihan1;
            pilihan = 0;
            System.out.println("==========Bab Soal Fisika=========");
            System.out.println("1. UTBK");
            System.out.println("2. USBN");
            System.out.println("3. Kembali");
            System.out.println("====================================");
            System.out.println("Pilihan anda=");
            pilihan = input.nextInt();
            switch (pilihan) {

                case 1:
                    boolean urutan;
                    urutan = true;
                    int skor = 0;
                    while (urutan) {
                        int pilihan_soal;
                        System.out.println("\n");
                        System.out.println("=============Pilih Soal=================");
                        for (int i = 0; i < soalUTBK.size(); i++) {
                            System.out.println("No. " + (i + 1));
                        }
                        System.out.println("\nKerjakan Soal No:");
                        pilihan_soal = input.nextInt();

                        switch (pilihan_soal) {
                            case 1:
                                String Jawabanuser;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser = input1.nextLine();
                                if (Jawabanuser.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor++;
                                }

                                break;
                            case 2:
                                String Jawabanuser1;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser1 = input1.nextLine();
                                if (Jawabanuser1.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor++;
                                }
                                System.out.println("Skor Anda: " + skor);
                                urutan = false;
                                break;

                        }

                    }

                    break;
                case 2:
                    boolean urutan1;
                    urutan1 = true;
                    int skor1 = 0;
                    while (urutan1) {
                        int pilihan_soal;
                        System.out.println("\n");
                        System.out.println("=============Pilih Soal=================");
                        for (int i = 0; i < soalUTBK.size(); i++) {
                            System.out.println("No. " + (i + 1));
                        }
                        System.out.println("\nKerjakan Soal No:");
                        pilihan_soal = input.nextInt();

                        switch (pilihan_soal) {
                            case 1:
                                String Jawabanuser;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser = input1.nextLine();
                                if (Jawabanuser.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor1++;
                                }

                                break;
                            case 2:
                                String Jawabanuser1;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser1 = input1.nextLine();
                                if (Jawabanuser1.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor1++;
                                }
                                System.out.println("=================== ");
                                System.out.println("Skor Anda: " + skor1);
                                urutan1 = false;
                                break;

                        }

                    }
                    break;

                case 3:
                    putusan = false;
            }

        }
    }

}
```
# Class SoalBiologi
```java
@Override
    public void SetSoal() {
        Scanner input = new Scanner(System.in);
        Scanner input1 = new Scanner(System.in);
        boolean putusan;
        putusan = true;

        soalUTBK.add(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget euismod erat. Lorem ipsum dolor?");
        pilihanA.add("Lorem ipsum");
        pilihanB.add("dolor sit ");
        pilihanC.add("amet, consectetur");
        jawaban.add("A");

        soalUTBK.add(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget euismod erat. Lorem ipsum dolor?");
        pilihanA.add("Lorem ipsum");
        pilihanB.add("dolor sit ");
        pilihanC.add("amet, consectetur");
        jawaban.add("B");
        while (putusan) {

            int pilihan, pilihan1;
            pilihan = 0;
            System.out.println("==========Bab Soal Fisika=========");
            System.out.println("1. UTBK");
            System.out.println("2. USBN");
            System.out.println("3. Kembali");
            System.out.println("====================================");
            System.out.println("Pilihan anda=");
            pilihan = input.nextInt();
            switch (pilihan) {

                case 1:
                    boolean urutan;
                    urutan = true;
                    int skor = 0;
                    while (urutan) {
                        int pilihan_soal;
                        System.out.println("\n");
                        System.out.println("=============Pilih Soal=================");
                        for (int i = 0; i < soalUTBK.size(); i++) {
                            System.out.println("No. " + (i + 1));
                        }
                        System.out.println("\nKerjakan Soal No:");
                        pilihan_soal = input.nextInt();

                        switch (pilihan_soal) {
                            case 1:
                                String Jawabanuser;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser = input1.nextLine();
                                if (Jawabanuser.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor++;
                                }

                                break;
                            case 2:
                                String Jawabanuser1;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser1 = input1.nextLine();
                                if (Jawabanuser1.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor++;
                                }
                                System.out.println("Skor Anda: " + skor);
                                urutan = false;
                                break;

                        }

                    }

                    break;
                case 2:
                    boolean urutan1;
                    urutan1 = true;
                    int skor1 = 0;
                    while (urutan1) {
                        int pilihan_soal;
                        System.out.println("\n");
                        System.out.println("=============Pilih Soal=================");
                        for (int i = 0; i < soalUTBK.size(); i++) {
                            System.out.println("No. " + (i + 1));
                        }
                        System.out.println("\nKerjakan Soal No:");
                        pilihan_soal = input.nextInt();

                        switch (pilihan_soal) {
                            case 1:
                                String Jawabanuser;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser = input1.nextLine();
                                if (Jawabanuser.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor1++;
                                }

                                break;
                            case 2:
                                String Jawabanuser1;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser1 = input1.nextLine();
                                if (Jawabanuser1.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor1++;
                                }
                                System.out.println("=================== ");
                                System.out.println("Skor Anda: " + skor1);
                                urutan1 = false;
                                break;

                        }

                    }
                    break;

                case 3:
                    putusan = false;
            }

        }
    }

}
```
#Class SoalKimia
```java
 @Override
    public void SetSoal() {
        Scanner input = new Scanner(System.in);
        Scanner input1 = new Scanner(System.in);
        boolean putusan;
        putusan = true;

        soalUTBK.add(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget euismod erat. Lorem ipsum dolor?");
        pilihanA.add("Lorem ipsum");
        pilihanB.add("dolor sit ");
        pilihanC.add("amet, consectetur");
        jawaban.add("A");

        soalUTBK.add(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget euismod erat. Lorem ipsum dolor?");
        pilihanA.add("Lorem ipsum");
        pilihanB.add("dolor sit ");
        pilihanC.add("amet, consectetur");
        jawaban.add("B");
        while (putusan) {

            int pilihan, pilihan1;
            pilihan = 0;
            System.out.println("==========Bab Soal Fisika=========");
            System.out.println("1. UTBK");
            System.out.println("2. USBN");
            System.out.println("3. Kembali");
            System.out.println("====================================");
            System.out.println("Pilihan anda=");
            pilihan = input.nextInt();
            switch (pilihan) {

                case 1:
                    boolean urutan;
                    urutan = true;
                    int skor = 0;
                    while (urutan) {
                        int pilihan_soal;
                        System.out.println("\n");
                        System.out.println("=============Pilih Soal=================");
                        for (int i = 0; i < soalUTBK.size(); i++) {
                            System.out.println("No. " + (i + 1));
                        }
                        System.out.println("\nKerjakan Soal No:");
                        pilihan_soal = input.nextInt();

                        switch (pilihan_soal) {
                            case 1:
                                String Jawabanuser;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser = input1.nextLine();
                                if (Jawabanuser.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor++;
                                }

                                break;
                            case 2:
                                String Jawabanuser1;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser1 = input1.nextLine();
                                if (Jawabanuser1.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor++;
                                }
                                System.out.println("Skor Anda: " + skor);
                                urutan = false;
                                break;

                        }

                    }

                    break;
                case 2:
                    boolean urutan1;
                    urutan1 = true;
                    int skor1 = 0;
                    while (urutan1) {
                        int pilihan_soal;
                        System.out.println("\n");
                        System.out.println("=============Pilih Soal=================");
                        for (int i = 0; i < soalUTBK.size(); i++) {
                            System.out.println("No. " + (i + 1));
                        }
                        System.out.println("\nKerjakan Soal No:");
                        pilihan_soal = input.nextInt();

                        switch (pilihan_soal) {
                            case 1:
                                String Jawabanuser;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser = input1.nextLine();
                                if (Jawabanuser.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor1++;
                                }

                                break;
                            case 2:
                                String Jawabanuser1;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser1 = input1.nextLine();
                                if (Jawabanuser1.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor1++;
                                }

                                System.out.println("=================== ");
                                System.out.println("Skor Anda: " + skor1);
                                urutan1 = false;
                                break;

                        }

                    }
                    break;

                case 3:
                    putusan = false;
            }

        }
    }

}
```
# Class SoalMatematika
```java
public class SoalMatematika extends Kumpulan_Soal {

    @Override
    public void SetSoal() {
```
# Class Matematika
```java
import java.util.Scanner;

public class SoalMatematika extends Kumpulan_Soal {

    @Override
    public void SetSoal() {
        Scanner input = new Scanner(System.in);
        Scanner input1 = new Scanner(System.in);
        boolean putusan;
        putusan = true;

        soalUTBK.add(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget euismod erat. Lorem ipsum dolor?");
        pilihanA.add("Lorem ipsum");
        pilihanB.add("dolor sit ");
        pilihanC.add("amet, consectetur");
        jawaban.add("A");

        soalUTBK.add(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget euismod erat. Lorem ipsum dolor?");
        pilihanA.add("Lorem ipsum");
        pilihanB.add("dolor sit ");
        pilihanC.add("amet, consectetur");
        jawaban.add("B");
        while (putusan) {

            int pilihan, pilihan1;
            pilihan = 0;
            System.out.println("==========Bab Soal Fisika=========");
            System.out.println("1. UTBK");
            System.out.println("2. USBN");
            System.out.println("3. Kembali");
            System.out.println("====================================");
            System.out.println("Pilihan anda=");
            pilihan = input.nextInt();
            switch (pilihan) {

                case 1:
                    boolean urutan;
                    urutan = true;
                    int skor = 0;
                    while (urutan) {
                        int pilihan_soal;
                        System.out.println("\n");
                        System.out.println("=============Pilih Soal=================");
                        for (int i = 0; i < soalUTBK.size(); i++) {
                            System.out.println("No. " + (i + 1));
                        }
                        System.out.println("\nKerjakan Soal No:");
                        pilihan_soal = input.nextInt();

                        switch (pilihan_soal) {
                            case 1:
                                String Jawabanuser;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser = input1.nextLine();
                                if (Jawabanuser.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor++;
                                }

                                break;
                            case 2:
                                String Jawabanuser1;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser1 = input1.nextLine();
                                if (Jawabanuser1.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor++;
                                }
                                System.out.println("===================");
                                System.out.println("Skor Anda: " + skor);
                                urutan = false;
                                break;

                        }

                    }

                    break;
                case 2:
                    boolean urutan1;
                    urutan1 = true;
                    int skor1 = 0;
                    while (urutan1) {
                        int pilihan_soal;
                        System.out.println("\n");
                        System.out.println("=============Pilih Soal=================");
                        for (int i = 0; i < soalUTBK.size(); i++) {
                            System.out.println("No. " + (i + 1));
                        }
                        System.out.println("\nKerjakan Soal No:");
                        pilihan_soal = input.nextInt();

                        switch (pilihan_soal) {
                            case 1:
                                String Jawabanuser;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser = input1.nextLine();
                                if (Jawabanuser.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor1++;
                                }

                                break;
                            case 2:
                                String Jawabanuser1;

                                System.out.println("No." + pilihan_soal + " " + soalUTBK.get(pilihan_soal - 1));
                                System.out.println("A. " + pilihanA.get(pilihan_soal - 1));
                                System.out.println("B. " + pilihanB.get(pilihan_soal - 1));
                                System.out.println("C. " + pilihanC.get(pilihan_soal - 1));
                                System.out.println("Jawaban: ");
                                Jawabanuser1 = input1.nextLine();
                                if (Jawabanuser1.equalsIgnoreCase(jawaban.get(pilihan_soal - 1))) {
                                    skor1++;
                                }
                                System.out.println("Skor Anda: " + skor1);
                                urutan1 = false;
                                break;

                        }

                    }
                    break;

                case 3:
                    putusan = false;
            }

        }
    }

}
```
# Class Fisika
```java
public class Fisika extends Materi {

    @Override
    public void SetMateri() {
```
# Class Biologi
```java
class Biologi extends Materi {

    @Override
    public void SetMateri() {
```
# Class Kimia
```java
public class Kimia extends Materi {

    @Override
    public void SetMateri() {
```

# No 7
Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

[Link Use case](https://gitlab.com/abdullahazzam2199/uts-pbo/-/blob/main/UTS%20PBO/Kumpulan_Use_Case.pdf)

# No 8
Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)

[Link Class Diagram](https://gitlab.com/abdullahazzam2199/uts-pbo/-/blob/main/UTS%20PBO/Class_Diagram_Zenius.jpeg)

# No 9
Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)

# No 10
Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)
